import "bootstrap/dist/css/bootstrap.min.css";
import AxiosLibrary from "./components/AxiosLibrary";
import Fetch from "./components/FetchAPI";

function App() {
  return (
    <div>
      <Fetch/>
      <hr></hr>
      <AxiosLibrary/>
    </div>
  );
}

export default App;