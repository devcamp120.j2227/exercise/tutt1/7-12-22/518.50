import { Component } from "react";

class Fetch extends Component {
    fetchAPI = async (url, requestOptions) => {
        let response = await fetch(url, requestOptions);
        let data = await response.json();

        return data
    }

    getAllHandler = () => {
        var requestOptions = {
            method: "GET",
            redirect: "follow"
        }
        var url = "http://203.171.20.210:8080/devcamp-pizza365/orders";
        this.fetchAPI(url, requestOptions)
            .then((response) => {
                console.log(response)
            })
            .catch((error) => {
                console.log(error)
            })
    }

    createHandler = () => {
        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");

        var raw = JSON.stringify({
            "kichCo": "M",
            "duongKinh": "25",
            "suon": 4,
            "salad": "300",
            "loaiPizza": "HAWAII",
            "idVourcher": "16512",
            "thanhTien": 200000,
            "giamGia": 20000,
            "idLoaiNuocUong": "PEPSI",
            "soLuongNuoc": 3,
            "hoTen": "Vũ Tiến Thượng",
            "email": "sdsds@asd.asd",
            "soDienThoai": "454554",
            "diaChi": "Hà Nội",
            "loiNhan": "Pizza đế dày",
            "trangThai": "open"
        });

        var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: raw,
            redirect: 'follow'
        };

        var url = "http://203.171.20.210:8080/devcamp-pizza365/orders";

        this.fetchAPI(url, requestOptions)
            .then((response) => {
                console.log(response)
            })
            .catch((error) => {
                console.error(error)
            })
    }

    getByIDHandler = () => {
        var requestOptions = {
            method: "GET",
            redirect: "follow"
        }
        var orderCode = "y6lBH4ABNm"
        var url = "http://203.171.20.210:8080/devcamp-pizza365/orders/" + orderCode;

        this.fetchAPI(url, requestOptions)
            .then((response) => {
                console.log(response)
            })
            .catch((error) => {
                console.log(error)
            })
    }

    updateHandler = () => {
        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");
        
        var raw = JSON.stringify({
            "trangThai": "confirmed"
        });
        var id = 1884;
        var requestOptions = {
          method: 'PUT',
          headers: myHeaders,
          body: raw,
          redirect: 'follow'
        };

        var url = "http://203.171.20.210:8080/devcamp-pizza365/orders/" + id;

        this.fetchAPI(url, requestOptions)
            .then((response) => {
                console.log(response)
            })
            .catch((error) => {
                console.error(error)
            })
    }

    checkVoucherByIDHandler = () => {
        var requestOptions = {
            method: "GET",
            redirect: "follow"
        }
        var voucherId = "12354";
        var url = "http://203.171.20.210:8080/devcamp-pizza365/voucher_detail/" + voucherId;

        this.fetchAPI(url, requestOptions)
            .then((response) => {
                console.log(response)
            })
            .catch((error) => {
                console.log(error)
            })
    }

    getAllDrinkListHandler = () => {
        var requestOptions = {
            method: "GET",
            redirect: "follow"
        }
        var url = "http://203.171.20.210:8080/devcamp-pizza365/drinks";
        this.fetchAPI(url, requestOptions)
            .then((response) => {
                console.log(response)
            })
            .catch((error) => {
                console.log(error)
            })
    }

    render() {
        return (
            <div className="container jumbotron bg-light mt-4">
                <div className="row">
                    <p>Test Page for JavaScript Task. F5 to run code.</p>
                </div>
                <div className="row mt-4">
                    <div className="col">
                        <button className="btn btn-primary" onClick={this.getAllHandler}>Call api get all orders!</button>
                        <button className="btn btn-info" onClick={this.createHandler}>Call api create orders!</button>
                        <button className="btn btn-success" onClick={this.getByIDHandler}>Call api get orders by id!</button>
                        <button className="btn btn-secondary" onClick={this.updateHandler}>Call api update orders!</button>
                        <button className="btn btn-danger" onClick={this.checkVoucherByIDHandler}>Call api check voucher by id!</button>
                        <button className="btn btn-success" onClick={this.getAllDrinkListHandler}>Call api get drink list!</button>
                    </div>
                </div>
            </div>
        )
    }
}

export default Fetch