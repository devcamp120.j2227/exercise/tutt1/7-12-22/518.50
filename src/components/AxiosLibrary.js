import { Component } from "react";
import axios from "axios";

class AxiosLibrary extends Component {
    axiosLibraryCallAPI = async (config) => {
        let response = await axios(config);

        return response
    }

    getByIDHandler = () => {
        var orderCode = "y6lBH4ABNm";
        var config = {
            method: 'get',
            url: 'http://203.171.20.210:8080/devcamp-pizza365/orders/' + orderCode,
            headers: { }
        };

        this.axiosLibraryCallAPI(config)
            .then((response) => {
                console.log(response)
            })
            .catch((error) => {
                console.error(error)
            })
    }

    getAllHandler = () => {
        var config = {
            method: 'get',
            url: 'http://203.171.20.210:8080/devcamp-pizza365/orders',
            headers: { }
        };

        this.axiosLibraryCallAPI(config)
            .then((response) => {
                console.log(response)
            })
            .catch((error) => {
                console.error(error)
            })
    }

    createHandler = () => {
        var data = JSON.stringify({
            "kichCo": "M",
            "duongKinh": "25",
            "suon": 4,
            "salad": "300",
            "loaiPizza": "HAWAII",
            "idVourcher": "16512",
            "thanhTien": 200000,
            "giamGia": 20000,
            "idLoaiNuocUong": "PEPSI",
            "soLuongNuoc": 3,
            "hoTen": "Vũ Tiến Thượng",
            "email": "sdsds@asd.asd",
            "soDienThoai": "454554",
            "diaChi": "Hà Nội",
            "loiNhan": "Pizza đế dày",
            "trangThai": "open"
        });
        
        var config = {
            method: 'post',
            url: 'http://203.171.20.210:8080/devcamp-pizza365/orders',
            headers: { 
                'Content-Type': 'application/json'
            },
            data : data
        };
          

        this.axiosLibraryCallAPI(config)
            .then((response) => {
                console.log(response)
            })
            .catch((error) => {
                console.error(error)
            })
    }

    updateHandler = () => {
        var id = 1884;
        var data = JSON.stringify({
            "trangThai": "confirmed"
        });
        
        var config = {
            method: 'put',
            url: 'http://203.171.20.210:8080/devcamp-pizza365/orders/' + id,
            headers: { 
                'Content-Type': 'application/json'
            },
            data : data
        };

        this.axiosLibraryCallAPI(config)
            .then((response) => {
                console.log(response)
            })
            .catch((error) => {
                console.error(error)
            })
    }

    checkVoucherByIDHandler = () => {
        var voucherId = "12354"
        var config = {
            method: 'get',
            url: 'http://203.171.20.210:8080/devcamp-pizza365/voucher_detail/' + voucherId,
            headers: { }
        };

        this.axiosLibraryCallAPI(config)
            .then((response) => {
                console.log(response)
            })
            .catch((error) => {
                console.error(error)
            })
    }

    getAllDrinkListHandler = () => {
        var config = {
            method: 'get',
            url: 'http://203.171.20.210:8080/devcamp-pizza365/drinks',
            headers: { }
        };

        this.axiosLibraryCallAPI(config)
            .then((response) => {
                console.log(response)
            })
            .catch((error) => {
                console.error(error)
            })
    }

    render() {
        return (
            <div className="container jumbotron bg-light mt-4">
                <div className="row">
                    <p>Test Page for JavaScript Task. F5 to run code.</p>
                </div>
                <div className="row mt-4">
                    <div className="col">
                        <button className="btn btn-primary" onClick={this.getAllHandler}>Call api get all orders!</button>
                        <button className="btn btn-info" onClick={this.createHandler}>Call api create orders!</button>
                        <button className="btn btn-success" onClick={this.getByIDHandler}>Call api get orders by id!</button>
                        <button className="btn btn-secondary" onClick={this.updateHandler}>Call api update orders!</button>
                        <button className="btn btn-danger" onClick={this.checkVoucherByIDHandler}>Call api check voucher by id!</button>
                        <button className="btn btn-success" onClick={this.getAllDrinkListHandler}>Call api get drink list!</button>
                    </div>
                </div>
            </div>
        )
    }
}

export default AxiosLibrary;